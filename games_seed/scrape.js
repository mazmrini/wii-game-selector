'use strict';

const Scraper = require('images-scraper');
const fs = require('fs');

let raw = fs.readFileSync('wii.json');
const games = JSON.parse(raw);
const google = new Scraper();

(async () => {
    const scraped = [];

    for (const game of games) {
        const result = await google.scrape(`${game} wii`, 1);

        scraped.push({
            name: game,
            imageUrl: result[0].url,
            console: "WII"
        });
    }

    console.log(`Found: ${scraped.length}/${games.length}`);

    fs.writeFileSync("./wii-seed.json", JSON.stringify(scraped));
})();
