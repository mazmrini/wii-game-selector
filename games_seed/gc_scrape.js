'use strict';

const Scraper = require('images-scraper');
const fs = require('fs');

let raw = fs.readFileSync('gamecube.json');
const games = JSON.parse(raw);
const google = new Scraper();

(async () => {
    let scraped = [];

    const step = 5;
    for (let i = 0; i < games.length; i += step) {
        const slice = games.slice(i, i + step);
        const mapped = await Promise.all(slice.map(async (game) => {
            try {
                const result = await google.scrape(`${game} gamecube`, 1);

                return {
                    name: game,
                    imageUrl: result[0].url,
                    console: "GAMECUBE"
                };
            }
            catch {
                return {
                    name: game,
                    imageUrl: "FAILED"
                };
            }
        }));
        scraped = [...scraped, ...mapped];
    }

    const failed = scraped.filter(s => s.imageUrl === "FAILED");
    const found = scraped.filter(s => s.imageUrl !== "FAILED");

    console.log(`Found: ${found.length}/${games.length}`);

    fs.writeFileSync("./gamecube-seed.json", JSON.stringify(found));
    if (failed.length > 0) {
        fs.writeFileSync("./gamecube-failed.json", JSON.stringify(failed.map(s => s.name)));
    }
})();
