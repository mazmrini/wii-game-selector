'use strict';

const fs = require('fs');
const axios = require('axios');

let raw = fs.readFileSync('gamecube-seed.json');
const games = JSON.parse(raw);

const username = process.env.WII_USERNAME;
const password = process.env.WII_PASSWORD;
if (!(username && password)) {
    console.log("Please set WII_USERNAME and WII_PASSWORD env vars");
    process.exit(-1);
}

(async () => {
    const result = await Promise.all(games.map(async (game, i) => {
        try {
            await axios.post("http://localhost:8080/api/games", game, {
                    auth: {
                        username,
                        password
                    }
            });

            return "SUCCESS";
        }
        catch {
            return {
                index: i,
                name: game.name
            };
        }
    }));

    const failed = result.filter(r => r !== "SUCCESS");
    console.log(`FAILED:\n${JSON.stringify(failed, null, 2)}`);
})();
