import xml.etree.ElementTree as ET
import json

tree = ET.parse('gamecube.xml')
root = tree.getroot()

data = [child.attrib['name'] for child in root if child.attrib['name'].endswith(".7z")]
data = list(set([d[:d.index(" (USA)")] for d in data if "USA" in d]))
data = sorted(data)

with open('gamecube.json', 'w') as f:
    json.dump(data, f, indent=4)
