import csv
import json

with open('wii.csv', newline='') as csvfile:
    spamreader = csv.reader(csvfile)
    data = [row[0] for row in spamreader]

data = [d[:d.index(" (USA")] for d in data[1:]]

with open('wii.json', 'w') as f:
    json.dump(data, f, indent=4)
