'use strict';

const fs = require('fs');
const axios = require('axios');

let raw = fs.readFileSync('wii-seed.json');
const games = JSON.parse(raw);

const username = process.env.WII_USERNAME;
const password = process.env.WII_PASSWORD;
if (!(username && password)) {
    console.log("Please set WII_USERNAME and WII_PASSWORD env vars");
    process.exit(-1);
}

(async () => {
    const failed = [];
    games.forEach(async (game, i) => {
        try {
            await axios.post("http://localhost:8080/api/games", game, {
                    auth: {
                        username,
                        password
                    }
            });
        }
        catch {
            failed.push(i);
        }
    });

    console.log(`FAILED indexes\n${failed}`);
})();
