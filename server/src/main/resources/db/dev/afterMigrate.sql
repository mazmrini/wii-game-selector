DELETE FROM game_choice;
DELETE FROM game;

-- Games null
INSERT INTO game (id, console, name, image_url) VALUES (
  '3c10f5c3-48a2-4ee2-8ed5-07ecd946d6b9',
  'WII',
  'Google',
  'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1024px-Google_%22G%22_Logo.svg.png'
);

INSERT INTO game (id, console, name, image_url) VALUES (
  'f1dca15f-eb58-4221-b7dc-5cfe0706f9d1',
  'WII',
  'Youtube',
  'https://logos-world.net/wp-content/uploads/2020/04/YouTube-Emblem.png'
);

INSERT INTO game (id, console, name, image_url) VALUES (
  '9a899ed4-5bb4-406b-a16d-9b067075be0b',
  'WII',
  'Facebook',
  'https://softwareengineeringdaily.com/wp-content/uploads/2019/05/facebook-logo-png-5a35528eaa4f08.7998622015134439826976.jpg'
);

INSERT INTO game (id, console, name, image_url) VALUES (
  'f72d9d39-0996-43de-b862-7237a67943a1',
  'GAMECUBE',
  'Wikipedia',
  'https://upload.wikimedia.org/wikipedia/commons/d/de/Wikipedia_Logo_1.0.png'
);

INSERT INTO game (id, console, name, image_url) VALUES (
  '6e6c3bfe-fcb7-4163-98ba-ad5e7110a9ab',
  'GAMECUBE',
  'Yahoo',
  'https://1000logos.net/wp-content/uploads/2017/05/Yahoo-logo.jpg'
);

-- Games YES
INSERT INTO game (id, console, name, image_url) VALUES (
  '784e95bb-ea84-4de4-b7d9-2359da9031ce',
  'WII',
  'GoogleYES',
  'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1024px-Google_%22G%22_Logo.svg.png'
);
INSERT INTO game_choice (game, choice) VALUES ('784e95bb-ea84-4de4-b7d9-2359da9031ce', 'YES');

INSERT INTO game (id, console, name, image_url) VALUES (
  '18551f59-7ab1-4e3e-9d76-8e0fdd19e53e',
  'WII',
  'YoutubeYES',
  'https://logos-world.net/wp-content/uploads/2020/04/YouTube-Emblem.png'
);
INSERT INTO game_choice (game, choice) VALUES ('18551f59-7ab1-4e3e-9d76-8e0fdd19e53e', 'YES');

INSERT INTO game (id, console, name, image_url) VALUES (
  '11830a03-85d5-46d3-b262-f470a1611730',
  'WII',
  'FacebookYES',
  'https://softwareengineeringdaily.com/wp-content/uploads/2019/05/facebook-logo-png-5a35528eaa4f08.7998622015134439826976.jpg'
);
INSERT INTO game_choice (game, choice) VALUES ('11830a03-85d5-46d3-b262-f470a1611730', 'YES');

INSERT INTO game (id, console, name, image_url) VALUES (
  '73f13826-9cb2-4843-8665-df760dd16da8',
  'GAMECUBE',
  'WikipediaYES',
  'https://upload.wikimedia.org/wikipedia/commons/d/de/Wikipedia_Logo_1.0.png'
);
INSERT INTO game_choice (game, choice) VALUES ('73f13826-9cb2-4843-8665-df760dd16da8', 'YES');

INSERT INTO game (id, console, name, image_url) VALUES (
  '72128635-842f-4ef6-a945-58a7224446b8',
  'GAMECUBE',
  'YahooYES',
  'https://1000logos.net/wp-content/uploads/2017/05/Yahoo-logo.jpg'
);
INSERT INTO game_choice (game, choice) VALUES ('72128635-842f-4ef6-a945-58a7224446b8', 'YES');

-- Games NO
INSERT INTO game (id, console, name, image_url) VALUES (
  'dce9be96-a080-4d1a-92f8-976b56705cca',
  'WII',
  'GoogleNO',
  'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1024px-Google_%22G%22_Logo.svg.png'
);
INSERT INTO game_choice (game, choice) VALUES ('dce9be96-a080-4d1a-92f8-976b56705cca', 'NO');

INSERT INTO game (id, console, name, image_url) VALUES (
  '49aa67ce-933c-4182-b9db-c31e39064aa9',
  'WII',
  'YoutubeNO',
  'https://logos-world.net/wp-content/uploads/2020/04/YouTube-Emblem.png'
);
INSERT INTO game_choice (game, choice) VALUES ('49aa67ce-933c-4182-b9db-c31e39064aa9', 'NO');

INSERT INTO game (id, console, name, image_url) VALUES (
  '4e68afa9-b3ff-4a73-ab48-1f69ac686e02',
  'WII',
  'FacebookNO',
  'https://softwareengineeringdaily.com/wp-content/uploads/2019/05/facebook-logo-png-5a35528eaa4f08.7998622015134439826976.jpg'
);
INSERT INTO game_choice (game, choice) VALUES ('4e68afa9-b3ff-4a73-ab48-1f69ac686e02', 'NO');

INSERT INTO game (id, console, name, image_url) VALUES (
  'e5572d98-f07b-4304-84d1-c3c0c5314249',
  'GAMECUBE',
  'WikipediaNO',
  'https://upload.wikimedia.org/wikipedia/commons/d/de/Wikipedia_Logo_1.0.png'
);
INSERT INTO game_choice (game, choice) VALUES ('e5572d98-f07b-4304-84d1-c3c0c5314249', 'NO');

INSERT INTO game (id, console, name, image_url) VALUES (
  'c6f6bf9d-3560-4b07-b5b3-6905078b1530',
  'GAMECUBE',
  'YahooNO',
  'https://1000logos.net/wp-content/uploads/2017/05/Yahoo-logo.jpg'
);
INSERT INTO game_choice (game, choice) VALUES ('c6f6bf9d-3560-4b07-b5b3-6905078b1530', 'NO');

-- Games MAYBE
INSERT INTO game (id, console, name, image_url) VALUES (
  '01c9b3c5-fed0-40d0-ab35-90dafee62b77',
  'WII',
  'GoogleMAYBE',
  'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1024px-Google_%22G%22_Logo.svg.png'
);
INSERT INTO game_choice (game, choice) VALUES ('01c9b3c5-fed0-40d0-ab35-90dafee62b77', 'MAYBE');

INSERT INTO game (id, console, name, image_url) VALUES (
  '8b548863-91f0-4c08-9546-1041fd3a16a8',
  'WII',
  'YoutubeMAYBE',
  'https://logos-world.net/wp-content/uploads/2020/04/YouTube-Emblem.png'
);
INSERT INTO game_choice (game, choice) VALUES ('8b548863-91f0-4c08-9546-1041fd3a16a8', 'MAYBE');

INSERT INTO game (id, console, name, image_url) VALUES (
  'f2114319-e75f-411a-bec6-e0a3f4398f43',
  'WII',
  'FacebookMAYBE',
  'https://softwareengineeringdaily.com/wp-content/uploads/2019/05/facebook-logo-png-5a35528eaa4f08.7998622015134439826976.jpg'
);
INSERT INTO game_choice (game, choice) VALUES ('f2114319-e75f-411a-bec6-e0a3f4398f43', 'MAYBE');

INSERT INTO game (id, console, name, image_url) VALUES (
  '54299b7f-6ff4-4d23-9e9b-d565412f5869',
  'GAMECUBE',
  'WikipediaMAYBE',
  'https://upload.wikimedia.org/wikipedia/commons/d/de/Wikipedia_Logo_1.0.png'
);
INSERT INTO game_choice (game, choice) VALUES ('54299b7f-6ff4-4d23-9e9b-d565412f5869', 'MAYBE');

INSERT INTO game (id, console, name, image_url) VALUES (
  '56d8d3ea-5472-41db-ba8a-65110fa52e41',
  'GAMECUBE',
  'YahooMAYBE',
  'https://1000logos.net/wp-content/uploads/2017/05/Yahoo-logo.jpg'
);
INSERT INTO game_choice (game, choice) VALUES ('56d8d3ea-5472-41db-ba8a-65110fa52e41', 'MAYBE');