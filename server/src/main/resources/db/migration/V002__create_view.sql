-- no games view
CREATE VIEW not_chosen_game AS
  SELECT g.* from game g
  LEFT JOIN game_choice gc
  ON g.id = gc.game
  WHERE gc.game IS NULL;
