-- UUID support
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Games
CREATE TABLE game (
  id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
  console varchar(255) NOT NULL,
  name varchar(255) NOT NULL UNIQUE,
  image_url varchar(4096) NOT NULL,
  UNIQUE (console, name)
);

CREATE UNIQUE INDEX game_console_name ON game (console, name);

--- Choice
CREATE TABLE game_choice (
  id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
  game uuid NOT NULL REFERENCES game (id) ON DELETE CASCADE UNIQUE,
  choice varchar(255) NOT NULL,
  UNIQUE (game, choice)
);
