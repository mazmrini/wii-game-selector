-- Drop unique constraint on (game.name) since constraint exists on (game, console)
ALTER TABLE game DROP CONSTRAINT game_name_key;
