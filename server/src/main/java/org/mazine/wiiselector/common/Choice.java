package org.mazine.wiiselector.common;

public enum Choice {
    YES,
    NO,
    MAYBE
}
