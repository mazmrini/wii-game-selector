package org.mazine.wiiselector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WiiSelectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(WiiSelectorApplication.class, args);
	}

}
