package org.mazine.wiiselector.config.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Component
public class RequestLogFilter implements HandlerInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestLogFilter.class);

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
        String url =Optional.ofNullable(request.getQueryString())
                .map(qs -> String.format("%s?%s", request.getRequestURI(), qs))
                .orElse(request.getRequestURI());
        LOGGER.info("[{}] {} {}", response.getStatus(), request.getMethod(), url);
    }
}
