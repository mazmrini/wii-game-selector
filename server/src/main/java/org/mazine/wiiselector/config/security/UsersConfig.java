package org.mazine.wiiselector.config.security;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class UsersConfig {
    @Value("${wii.users.user.username}")
    private String userUsername;

    @Value("${wii.users.user.password}")
    private String userPassword;

    @Value("${wii.users.admin.username}")
    private String adminUsername;

    @Value("${wii.users.admin.password}")
    private String adminPassword;
}
