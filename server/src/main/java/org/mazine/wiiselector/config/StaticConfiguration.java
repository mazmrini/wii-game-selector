package org.mazine.wiiselector.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

@Configuration
public class StaticConfiguration implements WebMvcConfigurer {
    private static final Logger LOGGER = LoggerFactory.getLogger(StaticConfiguration.class);

    @Value("${wii.frontend.dir}")
    private String frontendDir;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        forwardHomeToIndexHtml(registry);

        redirectToHomeWithSingleDirectoryPath(registry);
        redirectToHomeWithMultipleDirectoriesPathThatDoesNotStartWithApi(registry);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        Path path = Optional.ofNullable(frontendDir)
                .map(Path::of)
                .map(Path::toAbsolutePath)
                .orElseThrow(() -> {
                    LOGGER.error("No static path");
                    return new RuntimeException();
                });

        if (!Files.isDirectory(path)) {
            LOGGER.error("Path {} is not a directory", path);
            throw new RuntimeException();
        }

        LOGGER.info("Using static dir {}", path);

        registry.addResourceHandler("/**").addResourceLocations("file:" + path + "/");
    }

    private void forwardHomeToIndexHtml(ViewControllerRegistry registry) {
        registry.addViewController("/")
                .setViewName("forward:/index.html");
    }

    private void redirectToHomeWithSingleDirectoryPath(ViewControllerRegistry registry) {
        registry.addViewController("/{x:[\\w\\-]+}")
                .setViewName("redirect:/");
    }

    private void redirectToHomeWithMultipleDirectoriesPathThatDoesNotStartWithApi(ViewControllerRegistry registry) {
        registry.addViewController("/{x:^(?!api$).*$}/**/{y:[\\w\\-]+}")
                .setViewName("redirect:/");
    }
}
