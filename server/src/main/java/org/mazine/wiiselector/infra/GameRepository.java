package org.mazine.wiiselector.infra;

import org.mazine.wiiselector.common.Console;
import org.mazine.wiiselector.infra.models.Game;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface GameRepository extends CrudRepository<Game, UUID> {
    Optional<Game> findByNameAndConsole(String name, Console console);

    @Query(value =
            "SELECT * " +
            "FROM not_chosen_game g " +
            "LIMIT :size " +
            "OFFSET :page * :size",
           nativeQuery = true)
    List<Game> findAllWithoutChoice(@Param("page") int page, @Param("size") int pageSize);

    @Query(value = "SELECT count(g) FROM not_chosen_game g", nativeQuery = true)
    int countAllWithoutChoice();
}
