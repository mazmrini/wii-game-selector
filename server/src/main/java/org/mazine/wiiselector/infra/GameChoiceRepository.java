package org.mazine.wiiselector.infra;

import org.mazine.wiiselector.common.Choice;
import org.mazine.wiiselector.infra.models.GameChoice;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface GameChoiceRepository extends PagingAndSortingRepository<GameChoice, UUID> {
    Optional<GameChoice> findByGameId(UUID id);

    List<GameChoice> findByChoice(Choice choice, Pageable pageable);

    int countByChoice(Choice choice);
}
