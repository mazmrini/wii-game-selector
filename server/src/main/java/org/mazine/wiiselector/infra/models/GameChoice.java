package org.mazine.wiiselector.infra.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.mazine.wiiselector.common.Choice;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.UUID;

@Data
@Entity(name = "game_choice")
@NoArgsConstructor
public class GameChoice {
    @Id
    @GeneratedValue
    private UUID id;

    @OneToOne
    @JoinColumn(name = "game", referencedColumnName = "id")
    private Game game;

    @Enumerated(EnumType.STRING)
    private Choice choice;

    public GameChoice(Game game, Choice choice) {
        this.game = game;
        this.choice = choice;
    }
}
