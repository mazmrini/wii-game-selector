package org.mazine.wiiselector.infra.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.mazine.wiiselector.common.Console;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Data
@Entity(name = "game")
@NoArgsConstructor
public class Game {
    @Id
    @GeneratedValue
    private UUID id;

    @Enumerated(EnumType.STRING)
    private Console console;

    private String name;

    private String imageUrl;
}
