package org.mazine.wiiselector.api;

import org.mazine.wiiselector.api.dtos.ErrorDto;
import org.mazine.wiiselector.api.exceptions.AlreadyExistsException;
import org.mazine.wiiselector.api.exceptions.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ErrorController {
    private transient static Map<Class<?>, String> messages;

    static {
        messages = new HashMap<>();
        messages.put(IllegalArgumentException.class, "Bad argument");
        messages.put(AlreadyExistsException.class, "Resource already exists");
        messages.put(NotFoundException.class, "Resource not found");
    }

    @ExceptionHandler({
            AlreadyExistsException.class,
            IllegalArgumentException.class,
    })
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorDto badRequest(Exception ex) {
        return toDto(ex);
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorDto notFound(Exception ex) {
        return toDto(ex);
    }

    private ErrorDto toDto(Exception ex) {
        return new ErrorDto(messages.get(ex.getClass()));
    }
}
