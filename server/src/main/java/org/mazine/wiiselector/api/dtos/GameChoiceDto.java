package org.mazine.wiiselector.api.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.mazine.wiiselector.common.Choice;
import org.mazine.wiiselector.common.Console;

import java.util.UUID;

@Data
@NoArgsConstructor
public class GameChoiceDto {
    private UUID gameId;

    private Console console;

    private String name;

    private String imageUrl;

    private Choice choice;
}
