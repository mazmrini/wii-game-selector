package org.mazine.wiiselector.api.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChoicesCountDto {
    private int nbWithoutChoice;
    private int nbYes;
    private int nbMaybe;
    private int nbNo;
}
