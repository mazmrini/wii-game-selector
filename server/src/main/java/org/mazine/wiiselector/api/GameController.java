package org.mazine.wiiselector.api;

import org.mazine.wiiselector.api.dtos.ChoiceDto;
import org.mazine.wiiselector.api.dtos.ChoicesCountDto;
import org.mazine.wiiselector.api.dtos.CreateGameDto;
import org.mazine.wiiselector.api.dtos.GameChoiceDto;
import org.mazine.wiiselector.api.dtos.GamesChoiceDto;
import org.mazine.wiiselector.api.exceptions.AlreadyExistsException;
import org.mazine.wiiselector.api.exceptions.NotFoundException;
import org.mazine.wiiselector.common.Choice;
import org.mazine.wiiselector.infra.GameChoiceRepository;
import org.mazine.wiiselector.infra.GameRepository;
import org.mazine.wiiselector.infra.models.Game;
import org.mazine.wiiselector.infra.models.GameChoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class GameController {
    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private GameChoiceRepository gameChoiceRepository;

    @Secured("ROLE_USER")
    @GetMapping("/games")
    public GamesChoiceDto getGames(
            @RequestParam(defaultValue = "0") @Min(0) int page,
            @RequestParam(defaultValue = "10") @Min(1) int pageSize,
            @RequestParam(required=false) Choice choice
    ) {
        int nbSeen = (page + 1) * pageSize;
        GamesChoiceDto gamesChoiceDto = new GamesChoiceDto();
        List<GameChoiceDto> dtos;
        if (Optional.ofNullable(choice).isPresent()) {
            dtos = gameChoiceRepository
                    .findByChoice(choice, PageRequest.of(page, pageSize, Sort.by("game.name")))
                    .stream()
                    .map(this::toGameChoiceDto)
                    .collect(Collectors.toList());

            int total = gameChoiceRepository.countByChoice(choice);
            gamesChoiceDto.setHasNext(nbSeen < total);
        }
        else {
            dtos = gameRepository
                    .findAllWithoutChoice(page, pageSize)
                    .stream()
                    .map(this::toGameChoiceDto)
                    .collect(Collectors.toList());

            int total = gameRepository.countAllWithoutChoice();
            gamesChoiceDto.setHasNext(nbSeen < total);
        }

        gamesChoiceDto.setGames(dtos);
        gamesChoiceDto.setChoicesCount(getChoicesCount());

        return gamesChoiceDto;
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/games")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addGames(@RequestBody CreateGameDto dto) {
        boolean alreadyExists = gameRepository
                .findByNameAndConsole(dto.getName(), dto.getConsole()).isPresent();

        if (alreadyExists) {
            throw new AlreadyExistsException();
        }

        gameRepository.save(toGame(dto));
    }

    @Secured("ROLE_USER")
    @PutMapping("/games/{id}/choice")
    public ChoicesCountDto makeChoice(@PathVariable UUID id, @RequestBody ChoiceDto dto) {
        GameChoice gameChoice = gameChoiceRepository.findByGameId(id)
                .orElseGet(() -> {
                    Game game = gameRepository.findById(id)
                            .orElseThrow(NotFoundException::new);
                    return new GameChoice(game, dto.getChoice());
                });

        gameChoice.setChoice(dto.getChoice());
        gameChoiceRepository.save(gameChoice);

        return getChoicesCount();
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/games/choice")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteChoices() {
        gameChoiceRepository.deleteAll();
    }

    private GameChoiceDto toGameChoiceDto(Game game) {
        GameChoiceDto dto = new GameChoiceDto();
        dto.setGameId(game.getId());
        dto.setName(game.getName());
        dto.setConsole(game.getConsole());
        dto.setImageUrl(game.getImageUrl());

        return dto;
    }

    private GameChoiceDto toGameChoiceDto(GameChoice gameChoice) {
        GameChoiceDto dto = toGameChoiceDto(gameChoice.getGame());
        dto.setChoice(gameChoice.getChoice());

        return dto;
    }

    private Game toGame(CreateGameDto dto) {
        Game game = new Game();
        game.setName(dto.getName());
        game.setConsole(dto.getConsole());
        game.setImageUrl(dto.getImageUrl());

        return game;
    }

    private ChoicesCountDto getChoicesCount() {
        ChoicesCountDto dto = new ChoicesCountDto();
        dto.setNbWithoutChoice(gameRepository.countAllWithoutChoice());
        dto.setNbYes(gameChoiceRepository.countByChoice(Choice.YES));
        dto.setNbMaybe(gameChoiceRepository.countByChoice(Choice.MAYBE));
        dto.setNbNo(gameChoiceRepository.countByChoice(Choice.NO));

        return dto;
    }
}
