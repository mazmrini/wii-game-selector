package org.mazine.wiiselector.api.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class GamesChoiceDto {
    private ChoicesCountDto choicesCount;
    private List<GameChoiceDto> games;
    private boolean hasNext;
}
