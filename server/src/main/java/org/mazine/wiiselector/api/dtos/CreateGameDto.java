package org.mazine.wiiselector.api.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.mazine.wiiselector.common.Choice;
import org.mazine.wiiselector.common.Console;

@Data
@NoArgsConstructor
public class CreateGameDto {
    private Console console;

    private String name;

    private String imageUrl;
}
