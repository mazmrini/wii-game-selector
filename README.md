# Wii Game Selector

`wii game selector` allows you to help a friend chose which game he wants you to add on the wii you are about to mod.

## Requirements
- Java 11
- npm
- Docker

## Run
Create `postgres-prod.env`, `wii-prod.env` and `seed.env` from example files. Then,

```bash
# The build step is done locally
bin/runProd

# Once the server is running, seed the database using
bin/seed
``` 

## Client
Available commands:
```bash
cd client

npp install
npm start
npm run lint
npm run build
```

## Server
```bash
cd server

WII_FRONTEND_DIR=/abs/path/to/frontend/dist/folder ./mvnw spring-boot:run
``` 

### Available Spring profiles
`dev` uses seed data

`no-auth` removes the need for the basic auth

`auth` uses the basic auth, you still need to provide credentials

## Seed
Seed files already exist under the `games_seed`. Both `wii-seed.json` and `gamecube-seed.json` contain the
seed information that needs to be pushed into the server using:
```bash
# First define server admin credentials under (WII_USERNAME and WII_PASSWORD) 
npm run seed
npm run gc_seed
```

### Wii
```bash
cd games_seed
npm install

# The wii.csv file is the base of the chain
# wii.csv -> wii.json
python csvToJson.py

# Scrapes images from wii.json to ready the seed
# This step takes around 1h, it is done synchronously
# wii.json -> wii-seed.json
npm run scrape

# Seeds the server (it needs to be up)
# You need to define the server admin basic auth credentials
# wii-seed.json -> POST /games
WII_USERNAME=username WII_PASSWORD=password npm run seed
``` 

### Gamecube
```bash
cd games_seed
npm install

# The gamecube.xml file is the base of the chain
# gamecube.xml -> gamecube.json
python xmlToJson.py

# Scrapes images from gamecube.json to ready the seed
# This step takes around 10mins, it is an improvement from the wii one
# gamecube.json -> gamecube-seed.json
npm run gc_scrape

# Seeds the server (it needs to be up)
# You need to define the server admin basic auth credentials
# gamecube-seed.json -> POST /games
WII_USERNAME=username WII_PASSWORD=password npm run gc_seed

# Some manual manipulation is needed to remove Nintendo/ 
# from some game names
``` 

## Backup volume

Backup
```bash
mkdir backup

docker run --rm \
  -v wii-game-selector_pgdata_prod:/pgdata \
  -v $(pwd)/backup:/backup \
  busybox \
  tar cvf /backup/backup.tar /pgdata
```

Load backup
```bash
docker run --rm \
  -v $(pwd)/backup:/backup \
  -v wii-game-selector_pgdata_prod:/pgdata \
  busybox \
  tar -xvf /backup/backup.tar
```