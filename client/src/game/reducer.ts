import { ChoicesCount, Game } from "../services/models";
import { ReduxAction } from "../types";
import { Choice } from "../utils";
import {
    GAME_FETCH_STARTED,
    GAME_FETCH_SUCCEEDED,
    GAME_FETCH_FAILED,
    MAKE_GAME_CHOICE_SUCCEEDED,
    MAKE_GAME_CHOICE_FAILED
 } from "./types";

export interface GameReducerState {
    currentChoice: Choice;
    choicesCount: ChoicesCount;
    isFetching: boolean;
    fetchFailed: boolean;
    games: Game[];
    hasMoreGames: boolean;
    makeChoiceFailedIndex: number;
}

export const initialState: GameReducerState = {
    currentChoice: undefined,
    choicesCount: {
        nbWithoutChoice: 0,
        nbYes: 0,
        nbMaybe: 0,
        nbNo: 0
    },
    isFetching: false,
    fetchFailed: false,
    games: [],
    hasMoreGames: true,
    makeChoiceFailedIndex: -1,
};

export default function reducer(
    oldState: GameReducerState = initialState,
    action: ReduxAction
): GameReducerState {
    switch (action.type) {
        case GAME_FETCH_STARTED:
            const isNewChoice = action.choice !== oldState.currentChoice;

            return {
                ...oldState,
                isFetching: true,
                currentChoice: action.choice,
                games: isNewChoice ? [] : [...oldState.games],
                fetchFailed: false
            };
        case GAME_FETCH_SUCCEEDED:
            return {
                ...oldState,
                choicesCount: action.choicesCount,
                isFetching: false,
                fetchFailed: false,
                games: [...oldState.games, ...action.games],
                hasMoreGames: action.hasMoreGames
            };
        case GAME_FETCH_FAILED:
            return {
                ...oldState,
                isFetching: false,
                fetchFailed: true
            };
        case MAKE_GAME_CHOICE_SUCCEEDED:
            const updatedGames = [...oldState.games];
            updatedGames[action.index] = action.game;

            return {
                ...oldState,
                choicesCount: action.choicesCount,
                games: updatedGames,
                makeChoiceFailedIndex: -1
            };
        case MAKE_GAME_CHOICE_FAILED:
            return {
                ...oldState,
                makeChoiceFailedIndex: action.index
            };
        default:
            return oldState;
    }
}
