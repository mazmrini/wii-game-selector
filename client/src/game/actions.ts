import { ReduxDispatch, ThunkAction } from "../types";
import { Choice } from "../utils";
import { api } from "../services";
import {
    GAME_FETCH_STARTED,
    GAME_FETCH_SUCCEEDED,
    GAME_FETCH_FAILED,
    MAKE_GAME_CHOICE_SUCCEEDED,
    MAKE_GAME_CHOICE_FAILED
 } from "./types";
import { Game } from "../services/models";

export function fetchGames(choice: Choice, page: number): ThunkAction<Promise<void>> {
    return async (dispatch: ReduxDispatch): Promise<void> => {
        dispatch({
            type: GAME_FETCH_STARTED,
            choice
        });

        let fetchFn = () => api.getUnseenGames(page);
        if (choice) {
            fetchFn = () => api.getGamesByChoice(choice, page);
        }

        try {
            const gamesResult = await fetchFn();
            dispatch({
                type: GAME_FETCH_SUCCEEDED,
                games: gamesResult.games,
                hasMoreGames: gamesResult.hasNext,
                choicesCount: gamesResult.choicesCount,
            });
        }
        catch {
            dispatch({
                type: GAME_FETCH_FAILED
            });
        }
    };
}

export function makeChoice(index: number, game: Game): ThunkAction<void> {
    return async (dispatch: ReduxDispatch): Promise<void> => {
        try {
            const result = await api.makeChoice(game);

            dispatch({
                type: MAKE_GAME_CHOICE_SUCCEEDED,
                game,
                index,
                choicesCount: result
            });
        }
        catch {
            dispatch({
                type: MAKE_GAME_CHOICE_FAILED,
                index
            });
        }
    }
}
