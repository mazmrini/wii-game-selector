import { Game, ChoicesCount } from "../services/models";
import { Choice } from "../utils";

export const GAME_FETCH_STARTED = "GAME_FETCH_STARTED";
export const GAME_FETCH_SUCCEEDED = "GAME_FETCH_SUCCEEDED";
export const GAME_FETCH_FAILED = "GAME_FETCH_FAILED";
export const MAKE_GAME_CHOICE_SUCCEEDED = "MAKE_GAME_CHOICE_SUCCEEDED";
export const MAKE_GAME_CHOICE_FAILED = "MAKE_GAME_CHOICE_FAILED";

export interface GameFetchStartedAction {
    type: typeof GAME_FETCH_STARTED;
    choice: Choice;
}

export interface GameFetchSuccededAction {
    type: typeof GAME_FETCH_SUCCEEDED;
    games: Game[];
    hasMoreGames: boolean;
    choicesCount: ChoicesCount;
}

export interface GameFetchFailedAction {
    type: typeof GAME_FETCH_FAILED;
}

export interface MakeGameChoiceSucceededAction {
    type: typeof MAKE_GAME_CHOICE_SUCCEEDED;
    game: Game;
    index: number;
    choicesCount: ChoicesCount;
}

export interface MakeGameChoiceFailedAction {
    type: typeof MAKE_GAME_CHOICE_FAILED;
    index: number;
}

export type GameActions =
    | GameFetchStartedAction
    | GameFetchSuccededAction
    | GameFetchFailedAction
    | MakeGameChoiceSucceededAction
    | MakeGameChoiceFailedAction;
