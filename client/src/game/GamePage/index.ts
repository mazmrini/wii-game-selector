import { connect } from "react-redux";
import { Location } from "history";
import routes from "../../routes";
import { ReduxDispatch, RootReducerState } from "../../types";
import { Choice } from "../../utils";
import { fetchGames } from "../actions";
import { DispatchProps, StateProps, GamePage } from "./GamePage";

function mapDispatchToProps(dispatch: ReduxDispatch): DispatchProps {
    return {
        fetch: (choice: Choice, page: number) => dispatch(fetchGames(choice, page))
    };
}

interface OwnProps {
    location: Location;
}

const routeToChoice: Record<string, Choice> = {}
routeToChoice[routes.unseenGames] = undefined;
routeToChoice[routes.yesGames] = "YES";
routeToChoice[routes.maybeGames] = "MAYBE";
routeToChoice[routes.noGames] = "NO";

function mapStateToProps(state: RootReducerState, ownProps: OwnProps): StateProps {
    return {
        pageChoice: routeToChoice[ownProps.location.pathname],
        nbGames: state.game.games.length,
        hasMoreGames: state.game.hasMoreGames,
        isFetching: state.game.isFetching,
        fetchFailed: state.game.fetchFailed,
    };
}

export default connect(mapStateToProps, mapDispatchToProps) (GamePage);