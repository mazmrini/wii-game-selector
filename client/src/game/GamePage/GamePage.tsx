import React from "react";
import { 
    CircularProgress,
    Grid, Container,
    makeStyles,
    createStyles,
    Theme,
    Button
} from "@material-ui/core";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import CheckCircleRounded from "@material-ui/icons/CheckCircleRounded";
import InfoRounded from "@material-ui/icons/InfoRounded";
import ErrorRounded from "@material-ui/icons/ErrorRounded";
import { Choice } from "../../utils";
import GamesList from "./GamesList";

export interface StateProps {
    pageChoice: Choice;
    nbGames: number;
    hasMoreGames: boolean;
    isFetching: boolean;
    fetchFailed: boolean;
}

export interface DispatchProps {
    fetch: (choice: Choice, page: number) => void;
}

type GamePageProps = StateProps & DispatchProps;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            paddingTop: theme.spacing(4),
            paddingBottom: theme.spacing(10)
        },
        noGamesIcon: {
            color: theme.palette.info.dark,
            marginBottom: theme.spacing(1)
        },
        noGamesText: {
            color: theme.palette.info.dark,
            fontSize: "1.1rem"
        },
        errorIcon: {
            color: theme.palette.error.main,
            marginBottom: theme.spacing(1)
        },
        errorText: {
            color: theme.palette.error.main,
            fontSize: "1.1rem"
        },
        noMoreGamesIcon: {
            color: theme.palette.success.main,
            marginBottom: theme.spacing(1)
        },
        noMoreGamesText: {
            color: theme.palette.success.main,
            fontSize: "1.1rem"
        }
    })
);

export const GamePage = (props: GamePageProps) => {
    const classes = useStyles();
    const [nextPage, setNextPage] = React.useState<number>(0);

    React.useEffect(() => {
        props.fetch(props.pageChoice, 0);
        setNextPage(1);
    }, [props.pageChoice]);

    return (
        <Container maxWidth="sm" className={classes.container}>
            <Grid container direction="column" alignItems="stretch">
                {props.nbGames > 0 &&
                    <GamesList />
                }
                {props.isFetching &&
                    <Grid item container justify="center">
                        <CircularProgress />
                    </Grid>
                }
                {!props.isFetching && props.hasMoreGames &&
                    <Grid item container justify="center">
                        <Button
                          onClick={handleLoadMoreGames}
                          variant="outlined"
                        >
                            <ArrowDownward fontSize="large" />
                        </Button>
                    </Grid>
                }
                {!props.isFetching && !props.hasMoreGames &&
                    <Grid container item direction="column" alignItems="center">
                        <Grid item className={classes.noMoreGamesIcon}>
                            <CheckCircleRounded fontSize="large"/>
                        </Grid>
                        <Grid item className={classes.noMoreGamesText}>
                            C'est terminé!
                        </Grid>
                    </Grid>
                }
                {props.nbGames === 0 && !props.fetchFailed && !props.isFetching &&
                    <Grid container item direction="column" alignItems="center">
                        <Grid item className={classes.noGamesIcon}>
                            <InfoRounded fontSize="large"/>
                        </Grid>
                        <Grid item className={classes.noGamesText}>
                            Il n'y a pas de jeu ici!
                        </Grid>
                    </Grid>
                }
                {props.fetchFailed && !props.isFetching &&
                    <Grid container item direction="column" alignItems="center">
                        <Grid item className={classes.errorIcon}>
                            <ErrorRounded fontSize="large"/>
                        </Grid>
                        <Grid item className={classes.errorText}>
                            Il y a eu une erreur, tu peux réessayer!
                        </Grid>
                    </Grid>
                }
            </Grid>
        </Container>
    );

    function handleLoadMoreGames(): void {
        props.fetch(props.pageChoice, nextPage);
        setNextPage(nextPage + 1);
    }
};
