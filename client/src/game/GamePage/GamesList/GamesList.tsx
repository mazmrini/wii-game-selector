import React from "react";
import { Grid, makeStyles, createStyles, Theme } from "@material-ui/core";
import { Game } from "../../../services/models"
import { Choice } from "../../../utils";
import GameCard from "../GameCard";

export interface DispatchProps {
    makeChoice: (index: number, game: Game) => void;
}

export interface StateProps {
    games: Game[];
    makeChoiceFailedIndex: number;
}

type GamesListProps = DispatchProps & StateProps;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        gameCard: {
            paddingBottom: theme.spacing(10)
        },
    }),
);

export const GamesList = (props: GamesListProps) => {
    const classes = useStyles();

    return (
        <>
            {
                props.games.map((game, i) => {
                    return (
                        <Grid className={classes.gameCard} item key={`${i}-${game.choice}`}>
                            <GameCard
                              game={game}
                              makeChoice={makeChoiceFn(i, game)}
                              makeChoiceFailed={props.makeChoiceFailedIndex === i}
                            />
                        </Grid>
                    );
                })
            }
        </>
    );

    function makeChoiceFn(index: number, game: Game): (choice: Choice) => void {
        return (choice) => props.makeChoice(
            index,
            {
                ...game,
                choice: choice
            }
        );
    }
};
