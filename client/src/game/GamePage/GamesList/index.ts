import { connect } from "react-redux";
import { ReduxDispatch, RootReducerState } from "../../../types";
import { Game } from "../../../services/models";
import { makeChoice } from "../../actions";
import { DispatchProps, StateProps, GamesList } from "./GamesList";

function mapDispatchToProps(dispatch: ReduxDispatch): DispatchProps {
    return {
        makeChoice: (index: number, game: Game) => dispatch(makeChoice(index, game))
    };
}

function mapStateToProps(state: RootReducerState): StateProps {
    return {
        games: state.game.games,
        makeChoiceFailedIndex: state.game.makeChoiceFailedIndex
    };
}

export default connect(mapStateToProps, mapDispatchToProps) (GamesList);