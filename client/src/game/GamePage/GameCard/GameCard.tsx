import React from "react";
import { Card, CardContent, CardMedia, makeStyles, createStyles, Grid, IconButton, Theme } from "@material-ui/core";
import Error from "@material-ui/icons/Error";
import ConsoleIcon from "../../../common/icons/ConsoleIcon";
import { Game } from "../../../services/models";
import { Choice } from "../../../utils";
import ThumbUp from "../../../common/icons/ThumbUp";
import ThumbsUpDown from "../../../common/icons/ThumbsUpDown";
import ThumbDown from "../../../common/icons/ThumbDown";

interface GameCardProps {
    game: Game;
    makeChoice: (choice: Choice) => void;
    makeChoiceFailed: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        content: {
            background: "rgb(245, 245, 245)"
        },
        media: {
            height: 360,
            width: "80%",
            margin: "auto"
        },
        title: {
            fontSize: "1.3rem"
        },
        console: {
            height: "1.8rem"
        },
        consoleName: {
            marginLeft: 16,
            fontSize: "1.2rem"
        },
        errorText: {
            color: theme.palette.error.main
        }
    }),
);

export const GameCard = (props: GameCardProps) => {
    const classes = useStyles();

    return (
        <Card>
            <CardMedia
              className={classes.media}
              image={props.game.imageUrl}
            />
            <CardContent className={classes.content}>
                <Grid container direction="column" spacing={4}>
                    <Grid container item justify="space-between" alignItems="center">
                        <Grid xs={6} item>
                            <span className={classes.title}>{props.game.name}</span>
                        </Grid>
                        <Grid xs={6} item container className={classes.console} justify="flex-end" alignItems="center">
                            <Grid item className={classes.console}>
                                <ConsoleIcon console={props.game.console} />
                            </Grid>
                            <Grid item className={classes.consoleName}>
                                {consoleName()}
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container item justify="space-around" alignItems="center">
                        <Grid item>
                            <IconButton onClick={handleIconClick("YES")}>
                                <ThumbUp fontSize="large" choice={props.game.choice} />
                            </IconButton>
                        </Grid>
                        <Grid item>
                            <IconButton onClick={handleIconClick("MAYBE")}>
                                <ThumbsUpDown fontSize="large" choice={props.game.choice} />
                            </IconButton>
                        </Grid>
                        <Grid item>
                            <IconButton onClick={handleIconClick("NO")}>
                                <ThumbDown fontSize="large" choice={props.game.choice} />
                            </IconButton>
                        </Grid>
                    </Grid>
                    {props.makeChoiceFailed &&
                        <Grid container item alignItems="center" spacing={2}>
                            <Grid item>
                                <Error color="error" />
                            </Grid>
                            <Grid item className={classes.errorText}>
                                Erreur lors du choix.
                            </Grid>
                        </Grid>
                    }
                </Grid>
            </CardContent>
        </Card>
    );

    function handleIconClick(choice: Choice): () => void {
        if (props.game.choice !== choice) {
            return () => props.makeChoice(choice);
        }

        return () => {};
    }

    function consoleName(): string {
        let name = "Wii";
        if (props.game.console === "GAMECUBE") {
            name = "Gamecube";
        }

        return name;
    }
};