import React from "react";
import { Grid, makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
    centered: {
        marginTop: "25vh"
    }
});

export const CenteredContainer = (props: React.PropsWithChildren<{}>) => {
    const classes = useStyles();

    return (
        <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
            className={classes.centered}
        >
            <Grid item>
                {props.children}
            </Grid>
        </Grid>
    );
};
