import React from "react";
import { AppBar as MuiAppBar, Toolbar, makeStyles, createStyles, Theme, Grid, Badge } from "@material-ui/core";
import { Link } from "react-router-dom";
import routes from "../../routes";
import Unselected from "../icons/Unselected";
import ThumbUp from "../icons/ThumbUp";
import ThumbDown from "../icons/ThumbDown";
import ThumbsUpDown from "../icons/ThumbsUpDown";
import { Choice } from "../../utils";
import { ChoicesCount } from "../../services/models";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appbar: {
            marginBottom: theme.spacing(4),
            background: "white",
        },
        badgeUnselected: {
            fontSize: "0.5rem",
            background: 'darkgray'
        },
        badgeYes: {
            fontSize: "0.5rem",
            background: theme.palette.success.light
        },
        badgeMaybe: {
            fontSize: "0.5rem",
            background: theme.palette.info.light
        },
        badgeNo: {
            fontSize: "0.5rem",
            background: theme.palette.error.light
        }
    }),
);

export interface StateProps {
    choicesCount: ChoicesCount;
}

type AppBarProps = StateProps;

export const AppBar = (props: AppBarProps) => {
    const {nbWithoutChoice, nbYes, nbMaybe, nbNo} = props.choicesCount;
    const classes = useStyles();
    const [currentChoice, setCurrentChoice] = React.useState<Choice>(defaultChoice());

    return (
        <MuiAppBar className={classes.appbar} color="transparent" position="sticky">
            <Toolbar>
                <Grid container justify="center" spacing={6}>
                    {renderIcon(
                        routes.unseenGames,
                        nbWithoutChoice,
                        classes.badgeUnselected,
                        <Unselected choice={currentChoice} />
                    )}
                    {renderIcon(
                        routes.yesGames,
                        nbYes,
                        classes.badgeYes,
                        <ThumbUp choice={currentChoice} />
                    )}
                    {renderIcon(
                        routes.maybeGames,
                        nbMaybe,
                        classes.badgeMaybe,
                        <ThumbsUpDown choice={currentChoice} />
                    )}
                    {renderIcon(
                        routes.noGames,
                        nbNo,
                        classes.badgeNo,
                        <ThumbDown choice={currentChoice} />
                    )}
                </Grid>
            </Toolbar>
        </MuiAppBar>
    );

    function renderIcon(route: string, count: number, badgeClass: string, icon: JSX.Element): JSX.Element {
        const withBadge = (
            <Badge badgeContent={count} max={999} classes={{badge: badgeClass}} color="primary">
                {icon}
            </Badge>
        );

        if (routeToChoice(route) === currentChoice) {
            return (
                <Grid item>
                    {withBadge}
                </Grid>
            );
        }

        return (
            <Grid item>
                <Link to={route} onClick={handleIconClick(route)}>
                    {withBadge}
                </Link>
            </Grid>
        );
    }

    function handleIconClick(path: string): () => void {
        return () => {
            setCurrentChoice(routeToChoice(path));
        };
    }

    function defaultChoice(): Choice {
        return routeToChoice(window.location.href);
    }

    function routeToChoice(route: string): Choice {
        if (route.endsWith(routes.yesGames)) {
            return "YES";
        }
        if (route.endsWith(routes.noGames)) {
            return "NO";
        }
        if (route.endsWith(routes.maybeGames)) {
            return "MAYBE";
        }

        return undefined;
    }
};