import { connect } from "react-redux";
import { RootReducerState } from "../../types";
import { AppBar, StateProps } from "./AppBar";

function mapStateToProps(state: RootReducerState, ownProps): StateProps {
    console.log(ownProps);
    console.log(state);
    return {
        choicesCount: state.game.choicesCount
    };
}

export default connect(mapStateToProps) (AppBar);
