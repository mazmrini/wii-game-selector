import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core";
import MuiThumbDown from "@material-ui/icons/ThumbDown";
import { Choice } from "../../../utils";

interface ThumbDownProps {
    fontSize?: "large" | undefined;
    choice: Choice;
}


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        unselected: {
            color: "lightgray"
        },
        selected: {
            color: theme.palette.error.main
        }
    }),
);

export const ThumbDown = (props: ThumbDownProps) => {
    const classes = useStyles();

    return (
        <span className={props.choice === "NO" ? classes.selected : classes.unselected}>
            <MuiThumbDown fontSize={props.fontSize} />
        </span>
    );
};
