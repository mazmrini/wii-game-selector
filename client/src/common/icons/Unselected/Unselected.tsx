import React from "react";
import { makeStyles } from "@material-ui/core";
import MuiUnselected from "@material-ui/icons/AllInbox";
import { Choice } from "../../../utils";

interface UnselectedProps {
    fontSize?: "large" | undefined;
    choice: Choice;
}

const useStyles = makeStyles({
    unselected: {
        color: "lightgray"
    },
    selected: {
        color: "black"
    }
});

export const Unselected = (props: UnselectedProps) => {
    const classes = useStyles();

    return (
        <span className={props.choice == undefined ? classes.selected : classes.unselected}>
            <MuiUnselected fontSize={props.fontSize} />
        </span>
    );
};
