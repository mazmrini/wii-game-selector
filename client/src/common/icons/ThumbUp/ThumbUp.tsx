import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core";
import MuiThumbUp from "@material-ui/icons/ThumbUp";
import { Choice } from "../../../utils";

interface ThumbUpProps {
    fontSize?: "large" | undefined;
    choice: Choice;
}


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        unselected: {
            color: "lightgray"
        },
        selected: {
            color: theme.palette.success.main
        }
    }),
);

export const ThumbUp = (props: ThumbUpProps) => {
    const classes = useStyles();

    return (
        <span className={props.choice === "YES" ? classes.selected : classes.unselected}>
            <MuiThumbUp fontSize={props.fontSize} />
        </span>
    );
};
