import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core";
import MuiThumbsUpDown from "@material-ui/icons/ThumbsUpDown";
import { Choice } from "../../../utils";

interface ThumbDownProps {
    fontSize?: "large" | undefined;
    choice: Choice;
}


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        unselected: {
            color: "lightgray"
        },
        selected: {
            color: theme.palette.info.main
        }
    }),
);

export const ThumbsUpDown = (props: ThumbDownProps) => {
    const classes = useStyles();

    return (
        <span className={props.choice === "MAYBE" ? classes.selected : classes.unselected}>
            <MuiThumbsUpDown fontSize={props.fontSize} />
        </span>
    );
};
