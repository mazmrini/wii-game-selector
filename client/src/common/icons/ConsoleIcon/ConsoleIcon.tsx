import { makeStyles } from "@material-ui/core";
import React from "react";
import { GameConsole } from "../../../utils";

interface ConsoleIconProps {
    console: GameConsole;
}

const useStyles = makeStyles({
    icon: {
        height: "100%"
    }
});

const wiiIconUrl = "https://cdn4.iconfinder.com/data/icons/input-device/512/Wii-512.png";
const gamecubeIconUrl = "https://img.icons8.com/color/452/nintendo-gamecube.png";

export const ConsoleIcon = (props: ConsoleIconProps) => {
    const classes = useStyles();
    let url = gamecubeIconUrl;
    let alt = "gamecube";
    if (props.console === "WII") {
        url = wiiIconUrl;
        alt = "wii";
    }

    return <img className={classes.icon} src={url} alt={alt} />;
};
