import React from "react";
import {
    makeStyles,
    createStyles,
    Theme,
    Paper
} from "@material-ui/core";
import CenteredContainer from "../../common/CenteredContainer";
import LoginForm from "./LoginForm";

export interface DispatchProps {
    login: (username: string, password: string) => void;
}

export interface StateProps {
    isLoginIn: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            width: 250,
            paddingRight: theme.spacing(4),
            paddingLeft: theme.spacing(4),
            paddingTop: theme.spacing(4),
            paddingBottom: theme.spacing(4)
        }
    }),
);

export const LoginPage = () => {
    const classes = useStyles();

    return (
        <CenteredContainer>
            <Paper className={classes.paper} variant="outlined">
                <LoginForm />
            </Paper>
        </CenteredContainer>
    );
};
