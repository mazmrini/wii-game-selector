import { connect } from "react-redux";
import { ReduxDispatch, RootReducerState } from "../../../types";
import { login } from "../../actions";
import { DispatchProps, LoginForm, StateProps } from "./LoginForm";


function mapDispatchToProps(dispatch: ReduxDispatch): DispatchProps {
    return {
        login: (username: string, password: string) => dispatch(login(username, password))
    };
}

function mapStateToProps(state: RootReducerState): StateProps {
    return {
        isLoginIn: state.login.isLoading,
        loginFailed: state.login.loginFailed
    };
}

export default connect(mapStateToProps, mapDispatchToProps) (LoginForm);