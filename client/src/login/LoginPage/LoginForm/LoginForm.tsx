import React from "react";
import {
    makeStyles,
    createStyles,
    Theme,
    Grid,
    FormControl,
    IconButton,
    InputAdornment,
    InputLabel,
    OutlinedInput,
    Typography,
    Button,
    CircularProgress
} from "@material-ui/core";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

export interface DispatchProps {
    login: (username: string, password: string) => void;
}

export interface StateProps {
    isLoginIn: boolean;
    loginFailed: boolean;
}

type LoginFormProps = DispatchProps & StateProps;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        text: {
            color: theme.palette.primary.main
        },
        button: {
            marginTop: theme.spacing(1)
        },
        errorText: {
            color: theme.palette.error.main,
            fontSize: "0.9rem",
            marginTop: theme.spacing(1)
        }
    }),
);

export const LoginForm = (props: LoginFormProps) => {
    const classes = useStyles();
    const [username, setUsername] = React.useState<string>("");
    const [password, setPassword] = React.useState<string>("");
    const [showPassword, setShowPassword] = React.useState<boolean>(false);

    return (
        <form autoComplete="off">
            <Grid
              container
              direction="column"
              spacing={3}
              alignItems="stretch"
              justify="center"
            >
                <Grid item>
                    <Typography variant="h6" align="center" className={classes.text}>
                        Connexion
                    </Typography>
                </Grid>
                <Grid item>
                    <FormControl variant="outlined" fullWidth>
                        <InputLabel htmlFor="username">Nom d'utilisateur</InputLabel>
                        <OutlinedInput
                            required
                            id="username"
                            type="text"
                            value={username}
                            onChange={handleUsernameChange}
                            labelWidth={125}
                        />
                    </FormControl>
                </Grid>
                <Grid item>
                    <FormControl variant="outlined" fullWidth>
                        <InputLabel htmlFor="password">Mot de passe</InputLabel>
                        <OutlinedInput
                            required
                            id="password"
                            type={showPassword ? "text" : "password"}
                            value={password}
                            onChange={handlePasswordChange}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle-password-visibility"
                                        onClick={handleClickShowPassword}
                                        edge="end"
                                    >
                                        {showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            }
                            labelWidth={100}
                        />
                    </FormControl>
                </Grid>
                {props.isLoginIn &&
                    <Grid item container justify="center" className={classes.button}>
                        <CircularProgress />
                    </Grid>
                }
                {!props.isLoginIn &&
                    <Grid item container direction="column" className={classes.button}>
                        <Button
                        disabled={!canLogin()}
                        variant="contained"
                        color="primary"
                        disableElevation
                        onClick={onLoginClick}>
                            Se connecter
                        </Button>
                        {props.loginFailed &&
                            <Grid item className={classes.errorText}>
                                Erreur lors de la connexion...
                            </Grid>
                        }
                    </Grid>
                }
            </Grid>
        </form>
    );

    function handleClickShowPassword(): void {
        setShowPassword(!showPassword);
    }

    function handlePasswordChange(event: React.ChangeEvent<HTMLInputElement>): void {
        setPassword(event.target.value);
    }

    function handleUsernameChange(event: React.ChangeEvent<HTMLInputElement>): void {
        setUsername(event.target.value);
    }

    function onLoginClick(): void {
        if (canLogin()) {
            props.login(username, password);
        }
    }

    function canLogin(): boolean {
        return username.length > 0 && password.length > 0;
    }
};
