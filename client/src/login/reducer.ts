import axios from "axios";
import { ReduxAction } from "../types";
import { LocalStorage } from "../utils";
import {
    AUTO_LOGIN_DONE,
    LOGIN_STARTED,
    LOGIN_FAILED,
    LOGIN_SUCCEEDED
 } from "./types";

export interface LoginReducerState {
    autoLoginDone: boolean;
    isLoading: boolean;
    isLoggedIn: boolean;
    loginFailed: boolean;
}

export const initialState: LoginReducerState = {
    autoLoginDone: false,
    isLoading: false,
    isLoggedIn: false,
    loginFailed: false
};

export default function reducer(
    oldState: LoginReducerState = initialState,
    action: ReduxAction
): LoginReducerState {
    switch (action.type) {
        case AUTO_LOGIN_DONE:
            return {
                ...oldState,
                autoLoginDone: true,
                isLoading: false
            };
        case LOGIN_STARTED:
            return {
                ...oldState,
                isLoading: true
            };
        case LOGIN_SUCCEEDED:
            LocalStorage.setUsername(action.username);
            LocalStorage.setPassword(action.password);
            setupAxiosAuthentication(action.username, action.password);

            return {
                ...oldState,
                isLoading: false,
                isLoggedIn: true,
                loginFailed: false
            };
        case LOGIN_FAILED:
            return {
                ...oldState,
                isLoading: false,
                isLoggedIn: false,
                loginFailed: true
            };
        default:
            return oldState;
    }
}

function setupAxiosAuthentication(username: string, password: string): void {
    axios.interceptors.request.use(config => {
        config.auth = {
            username,
            password
        };

        return config;
    });
}
