import { ReduxDispatch, ThunkAction } from "../types";
import { api } from "../services";
import { LocalStorage } from "../utils";
import {
    AUTO_LOGIN_DONE,
    LOGIN_STARTED,
    LOGIN_FAILED,
    LOGIN_SUCCEEDED,
 } from "./types";

export function login(username: string, password: string): ThunkAction<Promise<void>> {
    return async (dispatch: ReduxDispatch): Promise<void> => {
        dispatch({
            type: LOGIN_STARTED,
        });

        try {
            await api.login(username, password);

            dispatch({
                type: LOGIN_SUCCEEDED,
                username,
                password
            });
        }
        catch {
            dispatch({
                type: LOGIN_FAILED,
            });
        }
    };
}

export function autoLogin(): ThunkAction<Promise<void>> {
    return async (dispatch: ReduxDispatch): Promise<void> => {
        const username = LocalStorage.getUsername();
        const password = LocalStorage.getPassword();

        try {
            if (username && password) {
                dispatch({
                    type: LOGIN_STARTED,
                });
                await api.login(username, password);

                dispatch({
                    type: LOGIN_SUCCEEDED,
                    username,
                    password
                });
            }
        }
        catch {}

        dispatch({
            type: AUTO_LOGIN_DONE
        });
    }
}
