export const AUTO_LOGIN_DONE = "AUTO_LOGIN_DONE";
export const LOGIN_STARTED = "LOGIN_STARTED";
export const LOGIN_SUCCEEDED = "LOGIN_SUCCEEDED";
export const LOGIN_FAILED = "LOGIN_FAILURE";

export interface AutoLoginDoneAction {
    type: typeof AUTO_LOGIN_DONE;
}

export interface LoginStartedAction {
    type: typeof LOGIN_STARTED;
}

export interface LoginSucceededAction {
    type: typeof LOGIN_SUCCEEDED;
    username: string;
    password: string;
}

export interface LoginFailedAction {
    type: typeof LOGIN_FAILED;
}

export type LoginActions =
| AutoLoginDoneAction
| LoginStartedAction
| LoginSucceededAction
| LoginFailedAction;
