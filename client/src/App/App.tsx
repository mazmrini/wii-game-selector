import React from "react";
import { CircularProgress } from "@material-ui/core";
import { Switch, Route, Redirect } from "react-router-dom";
import routes from "../routes";
import AppBar from "../common/AppBar";
import LoginPage from "../login/LoginPage";
import GamePage from "../game/GamePage";
import CenteredContainer from "../common/CenteredContainer";

export interface DispatchProps {
    autoLogin: () => Promise<void>;
}

export interface StateProps {
    autoLoginDone: boolean;
    isLoginIn: boolean;
    isLoggedIn: boolean;
}

type AppProps = DispatchProps & StateProps;

export const App = (props: AppProps) => {
    React.useEffect(() => {
        props.autoLogin();
    }, []);

    return (
        <>
            {!props.autoLoginDone && props.isLoginIn &&
                <CenteredContainer>
                    <CircularProgress />
                </CenteredContainer>
            }
            {props.autoLoginDone && !props.isLoggedIn &&
                <Switch>
                    <Route exact path={routes.login} component={LoginPage} />
                    <Redirect to={routes.login} />
                </Switch>
            }
            {props.autoLoginDone && props.isLoggedIn &&
                <>
                    <AppBar />
                    <Switch>
                        <Route exact path={routes.unseenGames} component={GamePage} />
                        <Route exact path={routes.yesGames} component={GamePage} />
                        <Route exact path={routes.maybeGames} component={GamePage} />
                        <Route exact path={routes.noGames} component={GamePage} />
                        <Redirect to={routes.unseenGames} />
                    </Switch>
                </>
            }
        </>
    );
};
