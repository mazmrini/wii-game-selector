import { connect } from "react-redux";
import { autoLogin } from "../login";
import { ReduxDispatch, RootReducerState } from "../types";
import { DispatchProps, StateProps, App } from "./App";

function mapDispatchToProps(dispatch: ReduxDispatch): DispatchProps {
    return {
        autoLogin: () => dispatch(autoLogin())
    };
}

function mapStateToProps(state: RootReducerState): StateProps {
    return {
        autoLoginDone: state.login.autoLoginDone,
        isLoggedIn: state.login.isLoggedIn,
        isLoginIn: state.login.isLoading
    };
}

export default connect(mapStateToProps, mapDispatchToProps) (App);