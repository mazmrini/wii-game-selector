import { ThunkDispatch, ThunkAction as VanillaThunkAction } from "redux-thunk";
import { LoginReducerState, LoginActions } from "./login";
import { GameReducerState, GameActions } from "./game";

export interface RootReducerState {
    login: LoginReducerState;
    game: GameReducerState;
}

export type ReduxAction =
  | LoginActions
  | GameActions;
export type ReduxDispatch = ThunkDispatch<RootReducerState, {}, ReduxAction>;
export type ThunkAction<T> = VanillaThunkAction<T, RootReducerState, {}, ReduxAction>;
