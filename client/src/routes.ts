export interface Routes {
    home: string;
    unseenGames: string;
    login: string;
    yesGames: string;
    noGames: string;
    maybeGames: string;
}

const routes: Routes = {
    home: "/",
    unseenGames: "/",
    login: "/login",
    yesGames: "/games/yes",
    noGames: "/games/no",
    maybeGames: "/games/maybe"
}

export default routes;
