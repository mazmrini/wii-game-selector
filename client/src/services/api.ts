import axios from "axios";
import path from "path";
import { Choice } from "../utils";
import { ChoicesCount, Game, Games } from "./models";

export class Api {
    pageSize: number;

    constructor(pageSize: number) {
        this.pageSize = pageSize;
    }

    public async login(username: string, password: string): Promise<void> {
        await axios.post<void>("/login", {}, {
            auth: {
                username,
                password
            }
        });
    }

    public async getUnseenGames(page: number): Promise<Games> {
        const endpoint = `${this.buildGamesEndpoint()}?page=${page}&pageSize=${this.pageSize}`;
        const result = await axios.get<Games>(endpoint);

        return result.data;
    }

    public async getGamesByChoice(choice: Choice, page: number): Promise<Games> {
        const endpoint = `${this.buildGamesEndpoint()}?choice=${choice}&page=${page}&pageSize=${this.pageSize}`;
        const result = await axios.get<Games>(endpoint)

        return result.data;
    }

    public async makeChoice(game: Game): Promise<ChoicesCount> {
        const endpoint = path.join(this.buildGamesEndpoint(), game.gameId, "choice");
        const result = await axios.put<ChoicesCount>(endpoint, {
            choice: game.choice
        });

        return result.data;
    }

    private buildGamesEndpoint(): string {
        return "/games";
    }
}

export default new Api(Number(process.env.REACT_APP_PAGE_SIZE || "10"));
