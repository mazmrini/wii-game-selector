import { Choice, GameConsole } from "../../utils";

export interface Game {
    gameId: string;
    console: GameConsole;
    name: string;
    imageUrl: string;
    choice: Choice;
}

export interface Games {
    games: Game[];
    hasNext: boolean;
    choicesCount: ChoicesCount;
}

export interface ChoicesCount {
    nbWithoutChoice: number;
    nbYes: number;
    nbMaybe: number;
    nbNo: number;
}
