import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Router } from "react-router-dom";
import { createHashHistory } from "history";
import axios from "axios"
import { ThemeProvider } from "@material-ui/core";
import theme from "./theme";
import wiiStore from "./store";
import App from "./App";

const hashHistory = createHashHistory();
hashHistory.listen(() => window.scrollTo(0, 0));

const baseUrl = process.env.REACT_APP_API_URL || window.origin;
axios.defaults.baseURL = `${baseUrl}/api`;

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Provider store={wiiStore}>
            <Router history={hashHistory}>
                <App />
            </Router>
        </Provider>
    </ThemeProvider>,
    document.getElementById("app")
);
