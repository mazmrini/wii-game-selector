export class LocalStorage {
    public static getUsername(): string | undefined {
        return localStorage["USERNAME"];
    }

    public static setUsername(value: string): void {
        localStorage["USERNAME"] = value;
    }

    public static getPassword(): string | undefined {
        return localStorage["PASSWORD"];
    }

    public static setPassword(value: string): void {
        localStorage["PASSWORD"] = value;
    }
}
