export type GameConsole = "WII" | "GAMECUBE";
export type Choice = "YES" | "NO" | "MAYBE" | undefined;
