import { applyMiddleware, createStore, combineReducers, Reducer } from "redux";
import { createLogger } from "redux-logger";
import thunkMiddleware from "redux-thunk";
import { RootReducerState, ReduxAction } from "./types";
import { reducer as login } from "./login";
import { reducer as game } from "./game";

const rootReducer: Reducer<RootReducerState, ReduxAction> = combineReducers({
    login,
    game
});

let middleware;
if (process.env.NODE_ENV === "production") {
    middleware = applyMiddleware(thunkMiddleware);
}
else {
    middleware = applyMiddleware(thunkMiddleware, createLogger());
}

const store = createStore(rootReducer, middleware);

export default store;