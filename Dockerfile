FROM openjdk:11-jre-slim

ENV WII_FRONTEND_DIR=/app/frontend

WORKDIR /app

COPY ./client/dist ./frontend
COPY ./server/target/wii-selector-app.jar ./app.jar

ENTRYPOINT ["java", "-jar", "app.jar"]
